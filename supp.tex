\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow}
\DeclareMathOperator*{\argmax}{argmax}


% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
%\setcounter{page}{4321}
\begin{document}

%%%%%%%%% TITLE
\title{Part-Stacked CNN \\ Supplementary Materials}

\maketitle

%%%%%%%%% BODY TEXT
\section{Implementation Details}

%-------------------------------------------------------------------------
\subsection{Practical tricks for training the model}
In our classification network which follows a two-stream architecture, we train two models M1 and M2 for initializing the object stream and part stream respectively.
Model M1 is obtained by fine-tuning from CaffeNet model pre-trained from ImageNet. Specifically, Bounding-box cropped images with size $227\times227$ from CUB-200-2011 dataset are used to fine-tune the CaffeNet, in which \textit{fc8} layer is replaced by a new layer with $200$ output units and randomly initialized; the parameters of other layers are directly inherited from the CaffeNet reference model.

For training M2 model, since the input of the part stream are images in doubled resolution $454\times454$, we first fine-tune an intermediate CaffeNet model that supports $454\times454$ inputs. It is achieved by replacing the original \textit{fc6} layer with a randomly initialized fully connected layer with larger size of inputs (from $6\times6\times256$ to $13\times13\times256$); the weights of other layers are inherited from M1 model trained on the object stream.
Based on this intermediate model, we further insert a convolutional layer \textit{conv5\_1} with $1\times1$ kernels on top of \textit{conv5} layer in CaffeNet architecture to conduct dimension reduction. The model is trained with the parameters of \textit{conv5\_1} and \textit{fc6} layers randomly initialized.

Practically, once these two initialized models M1 and M2 are successfully trained, we do not train all parts directly at one time. Instead, we train the network by gradually increasing the number of parts and re-finetune the resultant model. For example, we use two parts together with the object bounding box at the very beginning; once this two-part model is well trained, weights are used to initialize the network for the next training iteration with four parts and the bounding box.

%There are 16 fc6 layers in classification layers, while 15 fc6 layers for 15 parts are initialized randomly, the rest one receiving inputs from object stream is initialized by model M1.


\subsection{Implementation details for interpretation}
One of the most prominent features of the proposed PS-CNN method is that it can produce human-understandable interpretation manuals for fine-grained recognition. Here we detail the algorithm we use to perform interpretation using the proposed method.

Different from \cite{berg2013poof} who directly conducted one-on-one classification on object parts, the interpretation process of the proposed method is conducted in a relatively indirect way. Considering that using each object part by itself cannot lead to convincing classification results, we perform the analysis for interpretation on a combination of bounding box supervision and each single object part. The analysis is performed in two ways: a ``one-versus-rest'' comparison for denoting the most discriminative part to classify a subcategory from all other classes, and a ``one-versus-one'' comparison to find out the classification criteria of a subcategory with its most similar classes.

In particular, in the proposed architecture PS-CNN, the input of the part concatenation layer \emph{fc6} are $(K+1)$ feature maps, each of which corresponds to an object part or the whole object bounding box, with a dimension of $6\times6\times32$. The model parameters of the following two fully connected layers are trained using the concatenation of all parts. Therefore, in the inference process, the computation of each part before \emph{fc6} can be calculated separately, and then fed into the following two fully connected layers and ReLU functions. In other words, suppose there are $N$ testing images in the dataset and altogether $K$ classes, we can obtain a $N\times K$ score matrix $S$ for every possible combination of object parts; each element in the matrix $S_{ij}$ denotes the probability of an image belonging to a specific class. To study the impact of each object part, here we consider the combination of the object bounding box and a object part at each time, resulting in $K$ such score matrices termed $S^{(k)}$ respectively.

The ``one-versus-rest'' manual for an object category $k$ is then computed in the following procedure. For every part $p$, we compute the summation of the resultant scores of the category's positive samples.
The most discriminative part is then captured as the one with the largest accumulated score:
\begin{equation}
p_k^* = \argmax_p \sum_{i, y_i=k}S^{(p)}_{ip}.
\end{equation}

Analogously, the ``one-versus-one'' manual is obtained by computing as the part which results in the largest difference of prediction scores on two categories. Consider two classes $k$ and $l$. We first pick up the respective two rows in the score matrix $S$, and re-normalize it using the binary classification criterion as $S'$. Here $S'$ is in a dimension of $(N_k+N_l)\times2$, where $N_k$ and $N_l$ are the number of testing images labeled as the two classes respectively. Afterwards, the most discriminative part is given as $p_{k\rightarrow l}^*$:
\begin{equation}
p_{k\rightarrow l}^* = \argmax_p (\sum_{i, y_i=k}S'^{(p)}_{ip}+\sum_{j, y_j=l}S'^{(p)}_{jp})
\end{equation}



%------------------------------------------------------------------------
\section{Per-part classification accuracy}
We have studied the discriminative power of each object part through the part stream in the proposed architecture. Specifically, at each time, we discard the computation of all parts except one to conduct the classification process. Figure \ref{fig:accpp} shows the per-part accuracy in the CUB-200-2011 test set. The most powerful part is \textit{crown} - classification using this part alone achieves nearly $80\%$ of the discriminative power achieved by object-level supervision, which is quite impressive. The majority of object parts gets an accuracy of approximately $30\%$. It indicates that for fine-grained visual categorization, even if only a local part of the objects is examined, a reasonable performance can be achieved thanks to the powerful deep learning models. Based on the per-part accuracy, in our experiment, we iteratively insert the most discriminative object parts into the part stream and re-finetune the model to get a better result.


\begin{figure}[ht]
\begin{center}
\includegraphics[width=\linewidth]{accperpart}
\end{center}
   \caption{Classification accuracy using each object part only in the CUB-200-2011 test set, sorted in descending order. For reference, we also show the accuracy using bounding-box supervision in the comparison.}
\label{fig:accpp}
\end{figure}


%------------------------------------------------------------------------
\section{Additional interpretation results}
Due to the paper length limit, we only show one case of model interpretation of the proposed method. Here we list some additional experimental results in the supplementary. Figure \ref{fig:interp} shows another case of the model interpretation routine. This time the proposed method finds similar subcategories based on different factors including color and shape, while also being able to find the most discriminative parts for distinguishing similar categories.

\begin{figure*}[ht]
\begin{center}
\includegraphics[width=.9\linewidth]{interpret2}
\end{center}
   \caption{Interpretation example generated by the proposed method.}
\label{fig:interp}
\end{figure*}

Besides visualization results for individual test images, here we list the generated user manuals including some ``one-versus-rest'' comparisons for categories that are named by its unique property of a specific part and a selected subset of ``one-versus-one'' comparisons between highly similar object categories in Table \ref{tab:ovr} and Table \ref{tab:ovo}. Notice that the proposed method is able to detect the most discriminative object parts for distinguishing categories at most time.


\begin{table*}[t]
\begin{center}
\begin{tabular}{l|l}
\hline
Category Name & First $5$ prominent parts for ``one-versus-rest'' comparison\\
\hline
Red winged Blackbird & back, left wing, breast, left leg, right leg \\
\hline
Yellow headed Blackbird & crown, left eye, nape, breast, forehead \\
\hline
Painted Bunting & crown, right leg, belly, nape, left leg \\
\hline
Gray crowned Rosy Finch & crown, left eye, belly, right leg, back \\
\hline
Great Crested Flycatcher & crown, left wing, right wing, right eye, forehead \\
\hline
Yellow bellied Flycatcher & right wing, breast, left eye, right eye, beak\\
\hline
Pied billed Grebe & breast, crown, right eye, belly, beak \\
\hline
Rose breasted Grosbeak & left wing, breast, right eye, left eye, beak \\
\hline
Red breasted Merganser & crown, back, right eye, breast, nape \\
\hline
White crowned Sparrow & crown, right eye, nape, back, left eye \\
\hline
Black capped Vireo & left eye, forehead, left wing, belly, right eye\\
\hline
Blue headed Vireo & crown, right eye, nape, left eye, belly\\
\hline
White eyed Vireo & crown, left eye, right eye, forehead, back\\
\hline
Blue winged Warbler & crown, back, forehead, right eye, right wing\\
\hline
Red headed Woodpecker & crown, left eye, right eye, forehead, right wing \\
\hline

\end{tabular}
\end{center}
\caption{``One-versus-rest'' prediction manuals for some selected categories generated by the proposed method.}
\label{tab:ovr}
\end{table*}


\begin{table*}[t]
\begin{center}
\begin{tabular}{l|l}
\hline
Compared Category Name & First $5$ prominent parts for ``one-versus-one'' comparison\\
\hline
\multicolumn{2}{l}{\quad\quad Query Category: Philadelphia Vireo}\\
\hline
Black capped Vireo & crown, back, forehead, right eye, left eye \\
\hline
Blue headed Vireo & crown, left eye, forehead, right eye, nape \\
\hline
Red eyed Vireo & crown, back, right eye, forehead, breast \\
\hline
Warbling Vireo & back, forehead, right eye, left eye, right leg \\
\hline
White eyed Vireo & crown, right eye, forehead, left eye, right wing \\
\hline
Yellow throated Vireo & crown, left eye, nape, right eye, right leg\\
\hline
\hline
\multicolumn{2}{l}{\quad\quad Query Category: American Three toed Woodpecker} \\
\hline
Pileated Woodpecker & back, crown, belly, nape, forehead \\
\hline
Red bellied Woodpecker & crown, nape, back, belly, right wing \\
\hline
Red cockaded Woodpecker & crown, right eye, left eye, forehead, breast \\
\hline
Red headed Woodpecker & crown, nape, forehead, right eye, right leg \\
\hline
Downy Woodpecker & forehead, left wing, right eye, left eye, beak\\
\hline
\hline
\multicolumn{2}{l}{\quad\quad Query Category: Lazuli Bunting} \\
\hline
Indigo Bunting & crown, belly, right eye, forehead, nape \\
\hline
Painted Bunting & crown, right eye, forehead, breast, belly \\
\hline
\end{tabular}
\end{center}
\caption{``One-versus-one'' prediction manuals for some selected pairs of categories generated by the proposed method.}
\label{tab:ovo}
\end{table*}



%------------------------------------------------------------------------

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
