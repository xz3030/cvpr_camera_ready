\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\DeclareMathOperator*{\argmax}{argmax}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Predict and Explain: Part-Stacked CNN for Fine-grained Visual Categorization}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   We propose an approach for fine-grained visual categorization that is able to not only generate effective prediction results, but also present human-understandable explanations of the criterion for distinguishing fine-grained categories. Motivated by the fact that the difference between subordinate categories mostly lie on specific object parts, we propose a novel Part-Stacked CNN architecture to take advantage of manually-labeled strong part annotations. The proposed architecture consists of a fully-convolutional network to locate multiple object parts and a two-stream classification network that encodes object-level and part-level cues simultaneously. Experimental results on the CUB-200-2011 dataset reveal that the proposed architecture achieves state-of-the-art classification performance. Moreover, we also present practical examples of how the proposed approach helps people understand the fine-grained visual categorization process intuitively.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\label{sec:intro}
Fine-grained visual categorization aims to distinguish objects at the subordinate level, \eg, different species of birds \cite{welinder2010caltech,wah2011caltech,berg2014birdsnap}, pets \cite{khosla2011novel,parkhi2012cats}, flowers \cite{nilsback2008automated,angelova2013image} and cars \cite{stark2011fine,maji2013fine}. It is a highly challenging task due to the small inter-class variance caused by highly similar subordinate categories and the large intra-class variance by nuisance factors such as pose, viewpoint and occlusion. Inspiringly, huge progress has been made over the last few years \cite{wah2011multiclass,berg2014birdsnap,vedaldi2014understanding,krause2015fine,xu2015augmenting}, making fine-grained recognition techniques a large step closer to practical use spreading from wildlife observation to surveillance systems.

%It is widely acknowledged that the discriminative power between fine-grained categories mostly lies in the unique properties of object parts.

Whilst numerous attempts have been made to boost the classification accuracy of fine-grained visual categorization \cite{deng2013fine,chai2013symbiotic,branson2014bird,lin2015bilinear}, we argue that another important aspect of the problem has yet been severely overlooked, \ie, the ability to generate a human-understandable ``manual'' on how to distinguish fine-grained categories in detail. For example, volunteers for ecological protection may certainly benefit from an algorithm that could not only classify bird species accurately, but also provide brief instructions on how to distinguish a category from its most similar subspecies - \eg, a salient difference between a \textit{Ringed-beak gull} and a \textit{California gull} lies in the pattern on their beaks - with some intuitive illustration examples. Most of existing approaches to fine-grained recognition including weakly supervised methods \cite{xiao2014application,simon2015neural,lin2015bilinear} and human-in-the-loop routines \cite{wah2011multiclass,deng2013fine} are unable to comprehensively interpret their results or simply fail to scale for large systems.
Thus, a new approach to simultaneously implement and interpret fine-grained visual categorization is highly advocated.

It is widely acknowledged that the subtle differences between fine-grained categories mostly reside in the unique properties of object parts \cite{rosch1976basic,berg2013poof,chai2013symbiotic,maji2014part,zhang2014part}. Therefore, a practical solution to interpret classification results as human-understandable manuals is to discover classification criteria from object parts. This can be achieved by utilizing detailed part annotations available in some of the popular fine-grained datasets, such as 15 object part landmarks in the CUB-200-2011 dataset \cite{wah2011caltech}. However, the large number of object parts brings heavy computational burden for both part detection and classification. For this scenario, it is impractical to independently train a large CNN for each part and then ensemble them in a unified framework, as implemented in the state-of-the-art part-based R-CNN method \cite{zhang2014part}. Therefore, one would like to seek a method that follows the object-part-aware strategy to provide interpretable predicting criterions, while requiring significant less computational effort.

In this paper, we propose a new part-based CNN architecture for fine-grained visual categorization called Part-Stacked CNN (PS-CNN). Similar with previous fine-grained recognition approaches, the proposed method consists of a localization module to detect the object parts (``where pathway'') and a classification module to classify fine-grained categories at the subordinate level (``what pathway''). In particular, our localization network implements a fully-convolutional network (FCN) that is able to locate multiple object parts simultaneously and efficiently. The inferred part locations are fed into the classification network, in which a two-stream architecture is proposed to analyze images in both object-level (bounding boxes) and part-level (part landmarks). Motivated by \cite{felzenszwalb2010object}, we set the input image of the part stream twice the spatial resolution relative to the object stream to capture explicit part information. To improve model efficiency, in the part stream, we employ a shared feature extraction route for multiple parts, followed by a shallower network to classify subordinate categories based on each part. In specific, the computation of multiple object parts is first shared by forwarding the double-resolution input image using a shared set of convolution layers and pooling layers, which can be modeled as a generic feature extractor. Afterwards, a novel part crop layer is introduced to select a sub-region in the feature map based on the respective predicted part location for further computation on each part. Outputs of multiple object parts and bounding boxes are concatenated and fed into a set of fully connected layers to generate the final classification results. Except for categorical predictions for testing images, the proposed method also presents interpretable classification instructions obtained by computing the weight of each part in the classification procedure.

The contributions of this paper include: 1) To the best of our knowledge, the proposed method is the first attempt to both implement and explicitly interpret the process of fine-grained visual categorization without the use of human interactions. 2) We present a new part-based CNN architecture that employs an FCN to locate object parts and a two-stream classification network to model both object-level and part-level information. 3) The proposed architecture is fairly efficient, with a capacity of $xx$ frames/s to classify images at test time using $15$ object parts.
The effectiveness of the proposed method is demonstrated through systematic experiments on the Caltech-UCSD Birds-200-2011 \cite{wah2011caltech} dataset, in which we achieved $xxx\%$ classification accuracy. We also present practical examples of the human-understanding manuals generated by the proposed method for the task of fine-grained visual categorization.

%To the best of our knowledge, the proposed method is the first attempt to implement and explicitly explain the process of fine-grained visual categorization without the use of human interaction. The effectiveness of the proposed method is demonstrated through systematic experiments on the Caltech-UCSD Birds-200-2011 \cite{wah2011caltech} dataset, in which we achieved $xxx\%$ classification accuracy. We also present practical examples of the human-understanding instructions generated by the proposed method to distinguish fine-grained categories.

The rest of the paper are organized as follows. Section \ref{sec:relatedwork} summarizes related works. The proposed method including the localization network and classification network is described in Section \ref{sec:pscnn}.
%Section \ref{sec:explain} briefly presents the method to generate human-understanding instructions for fine-grained categorization based on the proposed architecture.
Detailed performance studies and analysis are conducted in Section \ref{sec:exp}. Section \ref{sec:conclusion} concludes the paper.



\section{Related Work}\label{sec:relatedwork}
\noindent\textbf{Fine-Grained Visual Categorization}.
A number of methods have been developed to classify object categories at the subordinate level. Recently, the best performing methods mostly seeked for improvement brought by the following three aspects: more discriminative features including deep CNNs for better visual representation \cite{bo2010kernel,sanchez2011fisher,krizhevsky2012imagenet,szegedy2014going,simonyan2014very}, explicit alignment approaches to eliminate pose displacements \cite{branson2014bird,gavves2014local}, and part-based methods to study the impact of object parts \cite{berg2013poof,maji2014part,zhang2014part}.
Among them, part-based methods are believed to be closely relevant to the nature of fine-grained recognition, in which the subtle differences between fine-grained categories mostly related to unique properties of object parts.

Some of the part-based methods \cite{berg2013poof,zhang2014part} employed strong annotations including bounding boxes, part landmarks or attributes from existing fine-grained recognition datasets \cite{wah2011caltech,parkhi2012cats,maji2013fine,vedaldi2014understanding}. While strong supervision significantly contributed to the performance boost, the expensive human labeling process motivated researchers to study weakly supervised fine-grained recognition without manually labeled part annotations, for which the basic idea is to discover object parts in an unsupervised fashion \cite{xiao2014application,simon2015neural,krause2015fine,lin2015bilinear}. Another line of research explored human-in-the-loop methods \cite{branson2010visual,deng2013fine,wah2014similarity} to identify the most discriminative regions for classifying fine-grained categories. Although such methods provided direct reference of how people do fine-grained recognition in real life, they were impossible to scale for large recognition systems due to the human interaction at test time.

Current state-of-the-art methods for fine-grained recognition are part-based R-CNN by Zhang \etal \cite{zhang2014part} and Bilinear CNN by Lin \etal \cite{lin2015bilinear}, which both employed a two-stream structure of part detection and part-based object classification. Our method shares the same framework of part-based R-CNN, while is far more efficient in both detection and classification phrases. As a result, the proposed method is able to employ all 15 object parts in the CUB-200-2011 dataset compared of 2 used in \cite{zhang2014part}, with slightly faster inference at test time. On the other hand, Lin \etal \cite{lin2015bilinear} argued that manually defined parts were sub-optimal for the task of object recognition, and thus proposed a bilinear model consisted of two streams whose roles were interchangeable as detectors or features. Although this design enjoyed the data-driven nature that could possibly lead to optimal classification performance, it also made the resultant model hard to interpret. On the contrary, our method aims to conduct fine-grained recognition from the perspective of both classification accuracy and model interpretability. As a result, we employ manually defined part information which has the potential to produce human-understandable classification criterions.\\

%\noindent\textbf{Datasets with Part Annotations and Attributes}.

\begin{figure*}[Ht]
\begin{center}
\includegraphics[width=.9\linewidth]{flowchart}
\end{center}
   \caption{Network architecture of the proposed Part-Stacked CNN model. The model consists of a fully convolutional network for part detection and a two-stream network for object classification. The input of the part stream is of twice the spatial resolution relative to the object stream. Multiple parts share the same feature extraction procedure, while being separated by a novel part crop layer to employ localization results. After conducting a dimension reduction layer, feature maps of all object parts and the bounding box are concatenated, following by three fully connected layers.}
\label{fig:architecture}
\end{figure*}

\noindent\textbf{Fully Convolutional Networks}.
Fully convolutional network (FCN) is a fast and effective approach to produce dense prediction with convolutional networks. Successful examples can be found on tasks including sliding window detection \cite{sermanet2013overfeat}, semantic segmentation \cite{long2015fully}, and human pose estimation \cite{tompson2014joint}. We find the problem of part landmark localization in fine-grained recognition could substantiality related to human pose estimation, in which a critical step is to detect a set of keypoints indicating multiple components of human body. Our method is also related to faster R-CNN \cite{ren2015faster}, which adopted an FCN to generate object proposals for object recognition, while our classification network is more complicated to model part-level information.




\section{Part-Stacked CNN} \label{sec:pscnn}
%As the key motivation of the proposed method is to produce an interpretable fine-grained recognition system, we employ strong part-level annotations that have the potential to provide human-understandable classification criterions.

The proposed Part-Stacked CNN architecture (PS-CNN) employs strong part-level annotations to provide human-understandable classification criterions.
In particular, consider a fine-grained dataset $\mathcal{I}=(X,Y)=\{x_i,y_i\}$ with $K$ classes and altogether $N$ images. Let's say there are $M$ object parts labeled in the dataset. Assume that $M$ is sufficient large to offer a complete set of object parts on which fine-grained categories usually different from each other. To relieve the human labeling effort, we assume the part annotations are in the simplest form, \ie a 2D key point is annotated at the center of each object part.

A pilot guidance here is to provide classification results that can be explicitly interpreted through the analysis of object parts, while being fairly efficient to deal with possibly large number of object parts. In accordance with the common framework for fine-grained recognition, the proposed Part-Stacked CNN architecture is decomposed into a \emph{Localization Network} (Section \ref{subsec:localization}) and a \emph{Classification Network} (Section \ref{subsec:classification}). We adopt CaffeNet \cite{jia2014caffe}, a modified implementation of the standard AlexNet \cite{krizhevsky2012imagenet}, as the basic structure of the network; deeper networks could potentially lead to better performance, but may also result in less efficiency. Figure \ref{fig:architecture} illustrates the overall network architecture of PS-CNN.


\begin{figure*}[t]
\begin{center}
\includegraphics[width=.8\linewidth]{fcn}
\end{center}
   \caption{Demonstration of the localization network. }
\label{fig:fcn}
\end{figure*}


\subsection{Localization Network} \label{subsec:localization}
The first stage of the proposed architecture is a localization network that aims to predict the location of $M$ object parts. Although the localization process of each part can be done independently using state-of-the-art object detection methods such as R-CNN \cite{girshick2014rich} as proposed in \cite{zhang2014part}, we find it too computational demanding in our case where the number of object parts is much larger. Motivated by recent progress of human pose estimation \cite{long2015fully} and semantic segmentation \cite{tompson2014joint}, we adopt a fully convolutional network (FCN) \cite{matan1995multi} to generate dense output feature maps for locating object parts.\\

\noindent\textbf{Fully convolutional network.}
A fully convolutional network is obtained by replacing the parameter-rich fully connected layers in standard CNN architectures by convolutional layers with $1\times1$ kernel size. Given the input of an RGB image, the output of a fully convolutional network is a \textit{feature map} in reduced dimension compared to the input image. The computation of each element in the feature map only corresponds to pixels inside a patch with fixed size in the input image, which is called its \textit{receptive field}. Note that a $1\times1$ convolution operator does not influence the receptive fields, as it can be modeled as doing a linear combination of the input channels. FCN is preferred in our framework due to the following three reasons: 1) feature maps generated by FCN can be directly utilized as the part locating results in the classification network, which we will detail in Section \ref{subsec:classification}; 2) results of multiple object parts can be obtained simultaneously using an FCN; 3) FCN is very efficient in both inference and learning. \\

% For example, each element of the feature map of \textit{conv5} layer in the CaffeNet corresponds to a 163x163 patch in the original image.\\

\noindent\textbf{Learning.} We model the part localization process as a multi-class classification problem on dense output spatial positions. In particular, suppose the output of the last convolutional layer in FCN is in the size of $h\times w\times d$, where $h$ and $w$ are spatial dimensions and $d$ is the number of channels. We set $d=M+1$, where $M$ is the number of object parts and $1$ denotes for an additional channel to model the background. To generate corresponding ground-truth labels in the form of feature maps, elements indexed by $h \times w$ spatial positions are labeled by their nearest object part, while elements that not close to any of the labeled parts (with an overlap $<0.5$ with respect to receptive field) are labeled as background.

%Ground-truth part annotations are transformed into the form of feature maps correspondingly.

A practical problem here is to determine the model depth and the size of input images for training the FCN. Generally speaking, layers at later stages carry more discriminative power and thus are more likely to generate reasonable localization results; however, the respective receptive fields are also much larger than those of previous layers. For example, the receptive field of \textit{conv5} layer in CaffeNet has a size of $163\times163$ compared to the $227\times227$ input image, which is too large to model an object part. An easy trick to deal with this problem is to upsample the input images so that the fixed-size receptive fields denoting object parts become relatively smaller compared to the whole object. In the proposed architecture, we upsample the input image to a double resolution of $454\times454$ and adopt \textit{conv5} layer to guarantee enough discriminative power.

The localization network in the proposed PS-CNN is illustrated in Figure \ref{fig:fcn}. The input of the FCN is a bounding-box-cropped RGB image, warped and resized into a fixed size of $454\times454$. The first five layers in the proposed localization network are identical to those in CaffeNet, which leads to a $27\times27\times256$ output after \textit{conv5} layer. We further place a $1\times1$ convolution layer with $512$ output channels as \textit{conv6}, and a classification layer with an output size of $(M+1)$ as another $1\times1$ convolution layer termed \textit{conv7}. The final loss function is a sum of softmax loss at all $27\times27$ positions by adopting a spatial preserving softmax that normalizes predictions at each spatial location of the feature map:

\begin{equation}\label{eqn:fcnloss}
L = \sum_{h=1}^{27}\sum_{w=1}^{27} \sigma(h,w,\hat{c}),
\end{equation}
where
\begin{displaymath}
\sigma(h,w,\hat{c}) = \frac{\exp(f_{conv7}(h,w,\hat{c}))}{\sum_{c=0}^M \exp(f_{conv7}(h,w,c))}.
\end{displaymath}
Here, $\hat{c}\in[0,1,...,M]$ is the part label of the patch at location $(h,w)$, label $0$ denotes background. $f_{conv7}(h,w,c)$ stands for the output of \textit{conv7} layer at spatial position $(h,w)$ and channel $c$.
\\
%We fine-tuned the model from CaffeNet using a learning rate of 0.001 and ran 60000 iterations.


\noindent\textbf{Inference.} The output of the learned FCN are $(M+1)$ feature maps in the size of $27\times27$. To remove isolated noise in the feature maps, we further introduce a Gaussian kernel $\mathcal{G}$ in the role of a smoothing operator. Let $g(h,w,c)=\sigma(h,w,c)*\mathcal{G}$. The location with the maximum response in each channel is modeled as the localization result and fed into the following classification network. Usually not all of the object parts are visible in an image. Therefore, we set a threshold $\mu$ that if the maximum response of a part is below $\mu$, we just simply discard the channel for this image in the classification network.

$$ (h_c^*,w_c^*)=\\
\left\{
\begin{aligned}
\argmax_{h,w} g(h,w,c) & \text{, if } g(h_c^*,w_c^*,c)>\mu \\
(-1,-1) & \text{, otherwise}
\end{aligned}
\right.
$$





\subsection{Classification network}\label{subsec:classification}
Taking the results of the pre-mentioned localization network as an input, the classification network in PS-CNN follows a two-stream architecture including a \emph{Part Stream} and a \emph{Object Stream}. We implement the proposed PS-CNN as a two stage framework, \ie after training the FCN, weights of the localization network are fixed when training the classification network.\\

\noindent\textbf{Part stream.}
The part stream acts as the core of the proposed PS-CNN architecture. Intuitively, one can train an individual CNN for each of the object parts given localization results, as proposed by Zhang \etal \cite{zhang2014part}. This method can definitely present highly interpretable results, since the CNNs are optimized only on regions according to a specific part. Although such method worked well for \cite{zhang2014part} who only employed two object parts, we argue that it is not applicable when the number of object parts is much higher because of its high time and space complexity.

In PS-CNN, we introduce two strategies to improve the efficiency of the part stream, mostly based on the idea of fully convolutional networks. The first one to make the model parameters of the first $5$ convolutional layers shared among all object parts, which can be regarded as a generic part-level feature extractor. Afterwards, a much shallower network is used for classifying objects according to each part. Since most of the model parameters indeed lie in fully connected layers at late stages, using a set of shared weights on the early convolutional layers does not produce significant drop of discriminative power for the part-specific CNNs.

Other than model parameter sharing, we also conduct a computational sharing strategy. Analogous to the localization network, the input images of the part stream are upsampled to a doubled resolution of $454\times454$. Forwarding the resized input images through the network will generate output feature maps of size $27\times 27$ after the \textit{conv5} layer. As a consequence, by far the computation of all object parts is completely shared. This is very efficient as the feature extraction procedure of the part stream only requires one pass through the convolution layers on a double-size input image.

\begin{figure}[h]
\begin{center}
\includegraphics[width=\linewidth]{partcrop}
\end{center}
   \caption{Demonstration of the part crop layer with 3 object parts.}
\label{fig:partcrop}
\end{figure}

The computation of each object part is then partitioned after performing the shared feature extraction procedure to model part-specific classification cues. In particular, we introduce a part crop layer to generate different inputs for the following computation of each part. As shown in Figure \ref{fig:partcrop}, the input for the part crop layer is a set of feature maps, \eg outputs after \textit{conv5} layer in our architecture, and the predicted 2D part locations from the previous localization network. For each part, the part crop layer extracts a local neighborhood region centered at the detected part location. Features outside the cropped region are simply dropped. In practice, we crop $6\times 6$ neighborhood regions out of the $27\times 27$ \textit{conv5} feature maps to match the output size of the object stream. The resultant receptive fields for the cropped feature maps has a width of $163+16\times 5=243$.

Let $f_{conv5}(h,w,c) \in \mathbb{R}^{27\times 27\times (M+1)}$ denotes the output of \textit{conv5} layer, $(h^*_c, w^*_c)$ be the predicted 2D locations for part $c$. The process of part crop layer can be formulated as:
\begin{equation}\label{eqn:fcnloss}
f_{out}(h,w,c) = f_{conv5}(h^*_c+h, w^*_c+w,c),
\end{equation}
where $h$ and $w$ range from $-2$ to $3$.
\\
%

\noindent\textbf{Object stream.}
The object stream captures bounding-box-level supervision for fine-grained recognition and performs as the baseline of the proposed system. It follows the general architecture of CaffeNet, in which the input of the network is a $227\times 227$ RGB image and the output of \textit{pool5} layer are $6\times 6$ feature maps.

%The output feature maps of the part stream and the object stream are further concatenated before feeding into fully connected layers.

We find the design of the two-stream architecture in PS-CNN analogous to the famous Deformable Part-based Models \cite{felzenszwalb2010object}, in which object-level features are captured through a root filter in coarser scale, while detailed part-level information is modeled by several part filters at finer scale.\\


\noindent\textbf{Dimension reduction and fully connected layers.}
We further introduce a $1\times1$ convolutional layer to each part and the object bounding box. This can be regarded as a low-rank projection of the model output and thus perform as a dimension reduction method that significantly reduce the number of parameters in the following fully connected layers. We find that directly initialize the weights of the additional convolution by PCA and projecting the 256 dimensional output to 32 in practice worsens the performance. Therefore, to enable domain-specific fine-tuning from pre-trained CNN model weights, we adopt an auxiliary CNN to initialize the weights for the additional convolutional layer.

After adopting dimension reduction, the outputs of the part stream and the object stream are concatenated and fed into a classification module consisting of three fully convolutional layers. We conduct the standard gradient descent to train the classification network. In fact, the classification network can be regarded as the combination of a DNN classifier consists of 3 fully connected layers and an over-complete set of CNN features from object bounding boxes and multiple parts.



%\subsection{Comparison to state-of-the-art architectures}\label{subsec:comparison}




\section{Experiments}\label{sec:exp}
We present experimental results and analysis of the proposed method in this section. Specifically, we will evaluate the performance through four different aspects: localization accuracy, classification accuracy, implementation efficiency and model interpretation.

\subsection{Dataset and implementation details}
Experiments were conducted on the widely used fine-grained classification benchmark the Caltech-UCSD Birds dataset (CUB-200-2011) \cite{wah2011caltech}. The dataset contains $200$ bird categories with roughly $30$ training images per category. We adopt strong supervision available in the dataset, \ie except for image-level labels and object bounding boxes, we also employ 2D key point part annotations of altogether $M=15$ object parts\footnote{The $15$ object parts are \textit{back, beak, belly, breast, crown, forehead, left eye, left leg, left wing, nape, right eye, right leg, right wing, tail}, and \textit{throat}.}. The labeled parts imply places where people usually focus on when being asked to classify fine-grained categories; thus provide valuable information for generating human-understandable systems.

We adopt the open-source package Caffe \cite{jia2014caffe} to implement the proposed Part-Stacked CNN architecture.
At both train time and test time, bounding-box cropped input images are warped to a fixed size of $512\times512$, random cropped into $454\times454$, and fed into the localization network and the part stream in the classification network. To guarantee synchronization in the computation of the two-stream classification network, we employ a max-pooling layer to downsample the $454\times454$ input of the part stream to $227\times227$ for the object stream.

%More detailed implementation tricks for the proposed method are presented in supplementary materials.

\subsection{Localization results}
Accurate localization contributes to the classification performance; it also leads to better interpretation for part-based methods. We quantitatively assess the localization correctness using three metrics. Since the object part annotations are not complete, \ie specific parts may not be visible in some images due to varied poses and occlusion, we compute both precision and recall on the detected key points in each image and then average over all test images. They are called \emph{MPK} (Mean Precision of Key points over images) and \emph{MRK} (Mean Recall of Key points over images) respectively. Note that by setting a threshold $\mu$ (Section \ref{subsec:localization}) that defines the minimum response of part detection, the proposed method is able to predict whether a part is visible or not. Suppose that image $I$ has $n_{gt}$ ground-truth parts, the proposed method predicts $n_{pd}$ parts existing in the image and $n_{tp}$ of them is correctly located, \emph{MPK} and \emph{MRK} are then calculated as:
\begin{eqnarray}
MPK = \frac{1}{N}\sum_{I\in\mathcal{I}}\frac{n_{tp}}{n_{pd}}, \nonumber\\
MRK = \frac{1}{N}\sum_{I\in\mathcal{I}}\frac{n_{tp}}{n_{gt}}.
\end{eqnarray}

We also adopt \emph{APK} (Average Precision of Key points) \cite{yang2013articulated} to explicitly study the localization performance of each part.
Following \cite{long2014convnets}, we consider a keypoint to be correctly predicted if the prediction lies within a Euclidean distance of $\alpha$ times the maximum of the bounding box width and height.
%Mean localization results are reported on multiple values of $\alpha\in \{0.1, 0.005, 0.0025\}$.
We set $\alpha=0.1$ in all the analysis below.

Table \ref{tab:locarc} details the effect of model architecture for the FCN-based localization network. Results reveal that by introducing an additional $1\times1$ convolutional layer after the first 5 layers in CaffeNet, the performance improvement was reasonably significant. The Gaussian kernel also contributed to the localization accuracy by removing isolated noise detections. The final localization network achieved an inspiring $81.2\%$ \emph{APK} on the test set of CUB-200-2011 dataset for 15 object parts.

\begin{table}
\begin{center}
\small
\begin{tabular}{l|c|c|c}
\hline
Model architecture & MPK & MRK & APK \\
\hline
conv5+cls & - & - & - \\
conv5+conv6(256)+cls & - & - & - \\
conv5+conv6(512)+cls & - & - & - \\
conv5+conv6(512)+cls+gaussian & 80.0 & 83.8 & 81.2 \\
\hline
\end{tabular}
\end{center}
\caption{Comparison of different model architectures on localization results. ``conv5'' stands for the first 5 convolutional layers in CaffeNet; conv6(256) stands for the additional $1\times1$ convolutional layer with 256 output channels; ``cls'' denotes the classification layer with $M+1$ output channels; ``gaussian'' represents a gaussian kernel for smoothing.}
\label{tab:locarc}
\end{table}

Furthermore, we present the per part \emph{APK} in Table \ref{tab:locapk}. An interesting phenomenon here is that parts residing near the head of the birds tended to be located more accurately. It turns out that the birds' head has relatively more stable structure with less deformations and lower probability to be occluded. On the contrary, parts that are highly deformable such as wings and legs got lower \emph{APK} values. Figure \ref{fig:loc} shows typical localization results of the proposed method.

\begin{table*}[ht]
\small
\begin{center}
\begin{tabular}{l|c|c|c|c|c|c|c|c}
\hline
part & throat & beak & crown & forehead & right eye & nape & left eye & back\\
\hline
APK & 0.908 & 0.894 & 0.894 & 0.885 & 0.861 & 0.857 & 0.850 & 0.807\\
\hline\hline
part & breast & belly & right leg & tail & left leg & right wing & left wing & overall\\
\hline
APK & 0.799 & 0.794 & 0.775 & 0.760 & 0.750 & 0.678 & 0.670 & 0.812\\
\hline
\end{tabular}
\end{center}
\caption{\emph{APK} for each object part in the CUB-200-2011 test set in descent order.}
\label{tab:locapk}
\end{table*}


\begin{figure*}[t]
\begin{center}
\includegraphics[width=.9\linewidth]{loc}
\end{center}
   \caption{Typical localization results on CUB-200-2011 test set. We show 6 of the 15 detected parts here. They are: beak (red), belly (green), crown (blue), right eye (yellow), right leg (magenta), tail (cyan).}
\label{fig:loc}
\end{figure*}


%\renewcommand{\labelenumi}{\alph{enumi})}
%\begin{enumerate}
%  \item Mean Precision of Key points over images (MPK).
%  \item Mean Recall of Key points over images (MRK).
%  \item Average Precision of Key points (APK).
%\end{enumerate}

%\begin{displaymath}
%\frac{d}{d vecx}=\sum_{p=1}^{15} S(x_p)^T \frac{dz}{dvecy_p}
%\end{displaymath}

\subsection{Classification results}

\subsection{Model interpretation}
















{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
