\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Predict and Explain: Part-Stacked CNN for Fine-grained Visual Categorization}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   We propose an approach for fine-grained visual categorization that is able to not only generate effective prediction results, but also present human-understandable explanations of the criterion for distinguishing fine-grained categories. Motivated by the fact that the difference between subordinate categories mostly lie on specific object parts, we propose a novel Part-Stacked CNN architecture to take advantage of manually-labeled strong part annotations. The proposed architecture consists of a fully-convolutional network to locate multiple object parts and a two-stream classification network that encodes object-level and part-level cues individually and uniformly. Experimental results on the CUB-200-2011 dataset reveal that the proposed architecture achieves state-of-the-art classification performance. Moreover, we also present practical examples of how the proposed approach helps people understand the fine-grained visual categorization process intuitively.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\label{sec:intro}
Fine-grained visual categorization aims to distinguish objects at the subordinate level, \eg, different species of birds \cite{welinder2010caltech,wah2011caltech,berg2014birdsnap}, pets \cite{khosla2011novel,parkhi2012cats}, flowers \cite{nilsback2008automated,angelova2013image} and cars \cite{stark2011fine,maji2013fine}. It is a highly challenging task due to the small inter-class variance caused by highly similar subordinate categories and the large intra-class variance by nuisance factors such as pose, viewpoint or location. Inspiringly, huge progress has been made over the last few years \cite{wah2011multiclass,berg2014birdsnap,vedaldi2014understanding,krause2015fine,xu2015augmenting}, making fine-grained recognition techniques a large step closer to practical use spreading from wildlife observation to surveillance systems.

%It is widely acknowledged that the discriminative power between fine-grained categories mostly lies in the unique properties of object parts.

Whilst numerous attempts have been made to boost the classification accuracy of fine-grained visual categorization \cite{deng2013fine,chai2013symbiotic,branson2014bird,lin2015bilinear}, we argue that another important aspect of the problem has yet been severely overlooked, \ie, the ability to generate a ``manual'' guiding people on how to distinguish fine-grained categories in detail. For example, volunteers for ecological protection may certainly benefit from an algorithm that could not only classify bird species accurately, but also provide brief instructions of how to distinguish a category from its most similar subspecies - \eg, a salient difference between a \textit{Ringed-beak gull} and a \textit{California gull} lies in the pattern on their beaks - with some intuitive illustration examples. Most of existing approaches to fine-grained visual categorization including weakly supervised methods \cite{xiao2014application,simon2015neural,lin2015bilinear} and human-in-the-loop routines \cite{wah2011multiclass,deng2013fine} are unable to comprehensively interpret their results or simply fail to scale for large systems.
A new approach to simultaneously implement and interpret fine-grained visual categorization is thus highly advocated.

It is widely acknowledged that the subtle differences between fine-grained categories mostly reside in the unique properties of object parts \cite{rosch1976basic,berg2013poof,chai2013symbiotic,maji2014part,zhang2014part}. Therefore, to interpret classification results as human-understandable manuals, a practical solution comes from utilizing detailed part annotations available in current fine-grained datasets, such as the 15 object part landmarks in the CUB-200-2011 dataset \cite{wah2011caltech}, to provide part-level recommendations for distinguishing subordinate categories. However, the large number of object parts brings heavy computational burden for both part detection and classification. As a result, it is impractical to separately train a large CNN for each part and then ensemble them in a unified framework, as implemented in the state-of-the-art part-based R-CNN method \cite{zhang2014part}. Therefore, one would like to seek a method that follows the object-part-aware strategy to explain the predicting criterions, while requiring significant less computational effort.

In this paper, we propose a new part-based CNN architecture for fine-grained visual categorization called Part-Stacked CNN (PS-CNN). Similar with previous fine-grained recognition approaches, the proposed method consists of a localization module to detect the object parts (``where pathway'') and a classification module to classify fine-grained categories in the subordinate level (``what pathway''). In particular, our localization network implements a fully-convolutional network (FCN) that is able to locate multiple object parts in constant time. The inferred part locations are fed into the classification network, in which a two-stream architecture is proposed to analyze images in both object-level (bounding boxes) and part-level (part landmarks). Motivated by \cite{felzenszwalb2010object}, we set the input image of the part stream twice the spatial resolution relative to the object stream to capture explicit part information. To guarantee efficient model inference, multiple object parts are first forwarded using a shared set of convolution layers and pooling layers which can be modeled as a generic feature extractor. Afterwards, each part is treated individually, starting from a novel crop layer that refines the input of a specific part as a local feature map centered at the respective predicted part location, followed by a shallower network to classify subordinate categories based on the part. Outputs of multiple object parts and bounding boxes are concatenated and fed into a set of fully connected layers to generate the final classification results. Interpretable classification instructions are obtained by computing the weight of each part in the process of classifying fine-grained categories by the proposed architecture.

The contributions of this paper include: 1) To the best of our knowledge, the proposed method is the first attempt to both implement and explicitly explain the process of fine-grained visual categorization without the use of human interaction. 2) We present a new part-based CNN architecture that employs an FCN to locate object parts and a two-stream classification network to model both object-level and part-level information. 3) The proposed architecture is fairly efficient, with a capacity of $xx$ frames/s to classify images at test time using $15$ object parts.
The effectiveness of the proposed method is demonstrated through systematic experiments on the Caltech-UCSD Birds-200-2011 \cite{wah2011caltech} dataset, in which we achieved $xxx\%$ classification accuracy. We also present practical examples of the human-understanding manuals generated by the proposed method for the task of fine-grained visual categorization.

%To the best of our knowledge, the proposed method is the first attempt to implement and explicitly explain the process of fine-grained visual categorization without the use of human interaction. The effectiveness of the proposed method is demonstrated through systematic experiments on the Caltech-UCSD Birds-200-2011 \cite{wah2011caltech} dataset, in which we achieved $xxx\%$ classification accuracy. We also present practical examples of the human-understanding instructions generated by the proposed method to distinguish fine-grained categories.

The rest of the paper are organized as follows. Section \ref{sec:relatedwork} summarizes related works. The proposed method including the localization network and classification network is described in Section \ref{sec:pscnn}. Section \ref{sec:explain} briefly presents the method to generate human-understanding instructions for fine-grained categorization based on the proposed architecture. Detailed performance studies and analysis are conducted in Section \ref{sec:exp}. Section \ref{sec:conclusion} concludes the paper.



\section{Related Work}\label{sec:relatedwork}
\noindent\textbf{Fine-Grained Visual Categorization}.
A number of methods have been developed to classify object categories at the subordinate level. Recently, the best performing methods mostly seeked for improvement brought by the following three aspects: more discriminative features including deep CNNs for better visual representation \cite{bo2010kernel,sanchez2011fisher,krizhevsky2012imagenet,szegedy2014going,simonyan2014very}, explicit alignment approaches to eliminate pose displacements \cite{branson2014bird,gavves2014local}, and part-based methods to model the impact of object parts \cite{berg2013poof,maji2014part,zhang2014part}. Among them, part-based methods are believed to be closely relevant to the nature of fine-grained recognition, in which the subtle differences between fine-grained categories mostly related to unique properties of object parts.

Some of the part-based methods \cite{berg2013poof,zhang2014part} employed strong annotations including bounding boxes, part landmarks or attributes from existing fine-grained recognition datasets \cite{wah2011caltech,parkhi2012cats,maji2013fine,vedaldi2014understanding}. While strong supervision significantly contributed to the performance boost, the expensive human labeling process motivated researchers to study weakly supervised fine-grained recognition without manually labeled part annotations, for which the basic idea is to discover object parts in an unsupervised fashion \cite{xiao2014application,simon2015neural,krause2015fine,lin2015bilinear}. Another line of research explored human-in-the-loop methods \cite{branson2010visual,deng2013fine,wah2014similarity} to identify the most discriminative regions for classifying fine-grained categories. Although such methods provided direct reference of how people do fine-grained recognition in real life, they were impossible to scale for large recognition systems due to the human interaction at test time.

Current state-of-the-art methods for fine-grained recognition are part-based R-CNN by Zhang \etal \cite{zhang2014part} and Bilinear CNN by Lin \etal \cite{lin2015bilinear}, which both employed a two-stream structure of part detection and part-based object classification. Our method shares the same framework of part-based R-CNN, while is far more efficient in both detection and classification phrases. As a result, the proposed method employs all 15 object parts in the CUB-200-2011 dataset compared of 2 used in \cite{zhang2014part}, with slightly faster inference at test time. On the other hand, Lin \etal \cite{lin2015bilinear} argued that manually defined parts were sub-optimal for the task of object recognition, and thus proposed a bilinear model consisted of two streams whose roles were interchangeable as parts or features. Although this design enjoyed data-driven nature that could possibly lead to optimal classification performance, it also made the resultant model hard to interpret. On the contrary, our method aims to conduct fine-grained recognition from the perspective of both classification accuracy and model interpretability. As a result, we employ manually defined part information which has the potential to produce human-understandable classification criterions.\\

%\noindent\textbf{Datasets with Part Annotations and Attributes}.

\begin{figure*}[Ht]
\begin{center}
\includegraphics[width=.9\linewidth]{flowchart}
\end{center}
   \caption{Network architecture of the proposed Part-Stacked CNN model. The model consists of a fully convolutional network for part detection and a two-stream network for classification. We use a double-resolution input image for the part stream. Multiple parts share the same feature extraction method, while being separated by a novel part crop layer integrating localization results. Feature maps of all object parts and the bounding box are concatenated before three fully connected layers.}
\label{fig:architecture}
\end{figure*}

\noindent\textbf{Fully Convolutional Networks}.
Fully convolutional network (FCN) is a fast and effective approach to produce dense prediction with convolutional networks. Successful examples can be found on tasks including sliding window detection \cite{sermanet2013overfeat}, semantic segmentation \cite{long2015fully}, and human pose estimation \cite{tompson2014joint}. We find the problem of part landmark localization in fine-grained recognition could substantiality related to human pose estimation, in which a critical step is to detect a set of keypoints indicating multiple components of human body. Our method is also related to faster R-CNN \cite{ren2015faster}, which adopted an FCN to generate object proposals for object recognition, while our classification network is more complicated to model part-level information.

\section{Part-Stacked CNN} \label{sec:pscnn}
As the key of the proposed method is to produce an interpretable fine-grained recognition system, we employ strong part-level supervision to introduce human-understandable annotations together with image-level labels and object-level bounding boxes. In particular, consider a fine-grained dataset with $K$ classes. Let's say there are $M$ object parts labeled in the dataset. To relieve the human labeling effort, we assume the part annotations are in the simplest form, \ie a key point is annotated at the center of each object part. The proposed Part-Stacked CNN architecture can be decomposed into a localization network (Section \ref{subsec:localization}) and a classification network (Section \ref{subsec:classification}). We adopt CaffeNet, a modified implementation of the standard AlexNet \cite{krizhevsky2012imagenet}, as the basic structure of the network; using deeper networks could potential lead to better performance, but may also result in less efficiency. Figure \ref{fig:architecture} illustrates an overview of the proposed method.


\subsection{Localization network}\label{subsec:localization}
The first stage of the proposed architecture is a localization network that aims to predict the location of $M$ object parts. Although the localization process of each part can be done independently using state-of-the-art object detection methods such as R-CNN \cite{girshick2014rich}, we find it too computational demanding for the proposed method, especially considering that the number of object parts could be possibly very large. Motivated by recent progress of human pose estimation \cite{long2015fully} and semantic segmentation \cite{tompson2014joint}, we adopt a fully convolutional network (FCN) to generate dense output feature maps for locating object parts.\\


\begin{figure*}[t]
\begin{center}
\includegraphics[width=.8\linewidth]{fcn}
\end{center}
   \caption{Demonstration of the localization network. }
\label{fig:fcn}
\end{figure*}

\noindent\textbf{Fully convolutional network.} A fully convolutional network is obtained by replacing the parameter-rich fully connected layers in standard CNN architectures by $1\times1$ convolutional layers. Given the input of an RGB image, the output of a fully convolutional network is a \textit{feature map} in reduced dimension compared to the input image. The computation of each element in the feature map only corresponds to pixels inside a patch with fixed size in the input image, which is called its \textit{receptive field}. Note that a $1\times1$ convolution operator does not influence the receptive fields, as it can be modeled as doing a linear combination of the input channels. FCN is preferred in our framework as the localization task due to the following two reasons: 1) feature maps generated by FCN can be directly utilized as the part locating results in the classification network, which we will detail in Section \ref{subsec:classification}; 2) FCN is very efficient in both inference and learning. \\

% For example, each element of the feature map of \textit{conv5} layer in the CaffeNet corresponds to a 163x163 patch in the original image.\\

\noindent\textbf{Learning.} We model the part localization process as a multi-class classification problem on dense output spatial positions. In particular, suppose the output of the last convolutional layer in FCN is in the size of $h\times w\times d$, where $h$ and $w$ are spatial dimensions and $d$ is the number of channels. To formulate the problem into a multi-class problem, we set $d=M+1$, where $M$ is the number of object parts and $1$ denotes for an additional channel to model the background. Ground-truth part annotations are transformed into the form of feature maps correspondingly. The elements indexed by $h \times w$ spatial positions are labeled by their nearest object part, while ones that are far from all the labeled parts (with an overlap $<0.5$ with respect to receptive field) are labeled as background.

A practical problem is to determine the specific convolution layer to use and the size of input image for training FCN. It is acknowledged that layers at later stages carry more discriminative power and thus are more probable to generate reasonable localization results. However, receptive fields of layers at later stages are also much larger. For example, each element of the feature map of \textit{conv5} layer in the CaffeNet corresponds to a $163\times163$ patch in the original image in size of $227\times227$, which could be too large to model an object part. In the proposed architecture, we adopt \textit{conv5} layer to guarantee enough discriminative power; meanwhile, we upsample the input image to a double resolution of $454\times454$, in which the receptive fields of \textit{conv5} layer become reasonably small.

The localization network in the proposed PSCNN is illustrated in Figure \ref{fig:fcn}. The input of the FCN is a bounding-box-cropped RGB image containing one object to recognize, warped and resized into a fixed size of $454\times454$. The first five layers in the proposed localization network is identical to those in CaffeNet, which leads to a $27\times27\times256$ output after \textit{conv5} layer. We further insert a $1\times1$ convolution layer with $512$ channels as \textit{conv6} and a classification layer with an output size of $(M+1)$ as another $1\times1$ convolution layer termed \textit{conv7}. A spatial reserving softmax operation is then conducted to normalize predictions at each spatial location in the feature map. The final loss function is a sum of softmax loss at all $27\times27$ positions.\\

%We fine-tuned the model from CaffeNet using a learning rate of 0.001 and ran 60000 iterations.


\noindent\textbf{Inference.} The output of the learned FCN are $(M+1)$ feature maps in the size of $27\times27$. To remove isolated noise points in the feature maps, we further introduce a Gaussian kernel $\mathcal{G}$ as a smoothing operator, which can be regarded as another convolution layer. The location with the maximum response in each channel is modeled as the localization result and fed into the following classification network. Meanwhile, usually not all of the object parts are visible in an image. We set a threshold $h$ that if the maximum response of a part is below $h$, we just simply discard the channel for this image in the classification network.




\subsection{Classification network}\label{subsec:classification}

\noindent\textbf{Object stream.}\\

\noindent\textbf{Part stream.}\\

\noindent\textbf{Fully connected layers.}

\subsection{Comparison to state-of-the-art architectures}\label{subsec:comparison}





















{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
